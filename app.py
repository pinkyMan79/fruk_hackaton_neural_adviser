from flask import Flask, request, jsonify
import numpy as np
from gensim.models import Word2Vec
from sklearn.linear_model import LogisticRegression

app = Flask(__name__)


def train_word2vec_model(all_courses):
    corpus = [course['description'].split() for course in all_courses]
    w2v_model = Word2Vec(sentences=corpus, vector_size=256, window=5, min_count=1, workers=4)
    w2v_model.train(corpus, total_examples=len(corpus), epochs=10)
    return w2v_model

def vectorize_text(text, w2v_model):
    words = text.split()
    vectorized_words = [w2v_model.wv[word] for word in words if word in w2v_model.wv]
    if vectorized_words:
        return np.mean(vectorized_words, axis=0)
    else:
        return np.zeros(256)

@app.route('/recommend_courses', methods=['POST'])
def recommend_courses():
    request_data = request.json

    all_courses = request_data.get('all_courses', [])

    passed_courses = [course for course in all_courses if course.get('passed')]
    not_passed_courses = [course for course in all_courses if not course.get('passed')]

    w2v_model = train_word2vec_model(all_courses)

    passed_courses_vectors = np.array([vectorize_text(course['description'], w2v_model) for course in passed_courses])
    not_passed_courses_vectors = np.array([vectorize_text(course['description'], w2v_model) for course in not_passed_courses])

    clf = LogisticRegression()
    X = np.vstack((passed_courses_vectors, not_passed_courses_vectors))
    y = np.array([1] * len(passed_courses) + [0] * len(not_passed_courses))
    clf.fit(X, y)

    user_vector = np.mean([vectorize_text(course['description'], w2v_model) for course in passed_courses], axis=0)

    print(user_vector, "as mention of user interests")

    user_vector_extended = np.append(user_vector, np.zeros(256))
    user_vector_extended = user_vector_extended.reshape(1, -1)
    user_vector_extended = user_vector_extended[:, :256]

    prediction = clf.predict(user_vector_extended)

    recommended_courses = [course['name'] for course, pred in zip(not_passed_courses, prediction) if pred == 1]

    return jsonify({'recommended_courses': recommended_courses})

if __name__ == '__main__':
    app.run(port=5000)
